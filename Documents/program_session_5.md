# Expressions régulières

+ http://apprendre-python.com/page-expressions-regulieres-regular-python

+ https://python.sdv.univ-paris-diderot.fr/#expressions-r%C3%A9guli%C3%A8res-et-parsing

aide : https://regex101.com/

source : https://docs.python.org/3/library/re.html

# Pandas

- https://jakevdp.github.io/PythonDataScienceHandbook/03.00-introduction-to-pandas.html

Exercices :
- https://github.com/guipsamora/pandas_exercises.git

> git clone https://github.com/guipsamora/pandas_exercises.git

Sources : 
- https://pandas.pydata.org/pandas-docs/stable/
- cookbook : https://pandas.pydata.org/pandas-docs/stable/cookbook.html


