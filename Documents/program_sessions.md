---
title: Formation « Débuter en Python »
author: Arnaud Mounier
numbersections: true
---

# Premiers pas

## Blocs d'instruction et identation

[Instructions et blocs](https://python.sdv.univ-paris-diderot.fr/01_introduction/)

## Variables : chaîne de charatères et nombres

[Variables](https://python.sdv.univ-paris-diderot.fr/02_variables/)

+ Exercices

[Variables](http://introtopython.org/var_string_num.html)

+ Exercices

## Affichage

[Display](https://python.sdv.univ-paris-diderot.fr/03_affichage/)

+ Exercices

# Strutures de données
## Les listes et les tuples

[Listes](https://python.sdv.univ-paris-diderot.fr/04_listes/)

+ Exercices

[Lists and tuples](http://introtopython.org/lists_tuples.html)

+ Exercices

## Les ensembles

À vous de jouer...

You turn...

## Les dictionnaires

[Dictionnaires et tuples](https://python.sdv.univ-paris-diderot.fr/13_dictionnaires_tuples/)

+ Exercice 13.3.1

[Dictionnaries](http://introtopython.org/dictionaries.html)

+ Exercices

# Rupture du flux d'instructions
## Les tests

[Tests](https://python.sdv.univ-paris-diderot.fr/06_tests/)

+ Excercices

[Tests](http://introtopython.org/if_statements.html)

+ Exercices

## Les boucles et les comparaisons

[Boucles et comparaisons](https://python.sdv.univ-paris-diderot.fr/05_boucles_comparaisons/)

+ Exercices

- [for statements](https://docs.python.org/3/tutorial/controlflow.html#for-statements)
- [while statements](http://introtopython.org/while_input.html)

+ Exercices

## S'échaper d'une boucle

[break et continue](https://docs.python.org/fr/3.6/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops)
[pass](https://docs.python.org/fr/3.6/tutorial/controlflow.html#pass-statements)

[break and continue](https://docs.python.org/3.6/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops)
[pass](https://docs.python.org/3.6/tutorial/controlflow.html#pass-statements)

## La fonction énumérer (enumarate)
[La fonction enumerate](https://docs.python.org/fr/3.6/library/functions.html#enumerate)

[enumerate](https://docs.python.org/3.5/library/functions.html#enumerate)

## Comprehension de listes

[Comprehension de listes](https://python.sdv.univ-paris-diderot.fr/21_remarques_complementaires/)

[List comprehension](http://introtopython.org/lists_tuples.html#Numerical-Comprehensions)

+ Exercices

# Les fonctions et les modules

- [Fonctions](https://python.sdv.univ-paris-diderot.fr/09_fonctions/)
- [Modules](https://python.sdv.univ-paris-diderot.fr/14_creation_modules/)

- [Functions](http://introtopython.org/introducing_functions.html)
- [More functions](http://introtopython.org/more_functions.html)

# Anticiper les erreurs internes

[Try ou except](https://python.sdv.univ-paris-diderot.fr/21_remarques_complementaires/)

[Try or except](https://docs.python.org/3.6/tutorial/errors.html#exceptions)

# Gestion des fichiers
## Les fichiers génériques
[Fichier](https://python.sdv.univ-paris-diderot.fr/07_fichiers/)
[Files](https://www.geeksforgeeks.org/file-handling-python/)

## Les fichiers CSV
[Fichiers CSV](https://docs.python.org/fr/3.7/library/csv.html)
[CSV files](https://docs.python.org/3.7/library/csv.html)

# Challenges

[Rosalind](http://rosalind.info/problems/locations/)

[PythonChallenge](http://www.pythonchallenge.com/)
