1. Écrire des programmes à destination d'êtres humains et non à des ordinateurs
; (a) Un programme ne doit pas exiger pour être compris d'avoir en tête plus d'une poignée d'éléments
; (b) Choisir des noms cohérents, clivants et explicites
; (c) Choisir un formatage et un style de code constant et cohérent
2. Laisser l'ordinateur travailler seul
; (a) Laisser à l'ordinateur les tâches répétitives
; (b) Faire un historique des commandes en vue de leur réutilisation
; (c) Utiliser un outil spécifique pour concevoir et exécuter les workflows
3. Faire des changements incrémentals
; (a) Avancer par petits pas avec des tests fréquents tout en rectifiant les objectifs si nécessaire
; (b) Utiliser un système de contrôle de version
; (c) Gérer tous vos fichiers (code compris) avec un système de contrôle de gestion
4. Ne faites aucune répétition
; (a) Chaque information doit avoir une seule représention faisant autorité dans tout le système
; (b) Modulariser votre code plutôt que faire du copier-coller
; (c) Réutiliser votre code plutôt que le réécrire
5. Prévoir les erreurs
; (a) Ajouter des opérations de vérification
; (b) Utiliser des bibliothèques de test relatives aux langages utilisés
; (c) Considérer les bugs comme des opportunités de test
; (d) Utiliser un débuggeur symbolique
6. Optimiser le programme seulement après une preuve de fonctionnement correct
; (a) Utiliser un logiciel de profilage pour identifier les points critiques
; (b) Privilégier les langages de plus haut niveau d'abstraction [le plus proche du langage naturel ndt]
7. La documentation doit détailler les intentions et les buts mais pas les recettes de cuisine [c'est pourquoi le code se doit d'être explicite ndt]; 
; (a) La documentation détaille et relie les buts mais dissimule des moyens [elle doit dire pourquoi mais pas comment ndt]
; (b) Remanier le code de préférence pour le rendre plus explicite
; (c) Inclure la documentation dans le code
8. Collaborer
; (a) Reviser son code avant de le fusionner (Use pre-merge code reviews)
; (b) Utiliser la programmation en binôme pour initier un nouveau rapidement ou pour faire face à un problème particulièrement difficile
; (c) Utiliser un outil de suivi
